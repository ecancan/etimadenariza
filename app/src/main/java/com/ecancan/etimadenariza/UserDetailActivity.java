package com.ecancan.etimadenariza;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ecancan.etimadenariza.Modules.Errors;
import com.ecancan.etimadenariza.Modules.Hash;
import com.ecancan.etimadenariza.Modules.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

public class UserDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private Button editUser;
    private Spinner spinnerView;
    private Spinner spinnerEmployerView;
    private RadioGroup radioGroupView;
    private RadioButton radioButtonView1;
    private RadioButton radioButtonView2;
    private RadioButton radioButtonView3;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private String getUserLevel;
    private Users users;
    private Toolbar toolbar;
    private SharedPreferences sp;
    private Users currentUsers;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private TextView userFullNameText;
    private TextView userLevetText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        Intent i = getIntent();
        users = (Users) i.getSerializableExtra("userDetail");
        toolbar = findViewById(R.id.userDetailToolbar);

        sp = this.getSharedPreferences("currentUserInfo", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("cuser", "");
        currentUsers = gson.fromJson(json, Users.class);

        toolbar.setTitle(users.getUserFullName());
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        setSupportActionBar(toolbar);

        navigationView = findViewById(R.id.navigationView);
        drawerLayout = findViewById(R.id.drawerLayout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,0,0);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        View slideHeder = navigationView.inflateHeaderView(R.layout.slider_menu_header);
        navigationView.setNavigationItemSelectedListener(this);
        userFullNameText = slideHeder.findViewById(R.id.header_user_full_name);
        userLevetText = slideHeder.findViewById(R.id.header_user_level);

        userFullNameText.setText(currentUsers.getUserFullName());
        userLevetText.setText(currentUsers.getUserLevel());

        spinnerView = findViewById(R.id.spinnerSign);
        spinnerEmployerView = findViewById(R.id.spinnerSignEmployer);
        radioGroupView = findViewById(R.id.radioGroupContainer);
        radioButtonView1 = findViewById(R.id.radioButton);
        radioButtonView2 = findViewById(R.id.radioButton2);
        radioButtonView3 = findViewById(R.id.radioButton3);
        editUser = findViewById(R.id.signButtonView);

        ArrayAdapter<String> userUnitAdapter = new ArrayAdapter<>(UserDetailActivity.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.userUnits));
        userUnitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerView.setAdapter(userUnitAdapter);

        int spinnerViewPosition = userUnitAdapter.getPosition(users.getUnit());
        spinnerView.setSelection(spinnerViewPosition);

        ArrayAdapter<String> employerAdapter = new ArrayAdapter<>(UserDetailActivity.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.employers));
        employerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEmployerView.setAdapter(employerAdapter);

        int spinnerEmployerViewPosition = employerAdapter.getPosition(users.getShift());
        spinnerEmployerView.setSelection(spinnerEmployerViewPosition);

        spinnerView.setEnabled(false);
        spinnerView.setClickable(false);
        spinnerEmployerView.setEnabled(false);
        spinnerEmployerView.setClickable(false);


        getSelectedRadioGroup(users.getUserLevel());

        radioGroupView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.radioButton){
                    spinnerView.setEnabled(false);
                    spinnerView.setClickable(false);
                    spinnerEmployerView.setEnabled(true);
                    spinnerEmployerView.setClickable(true);
                    spinnerView.setSelection(0);
                }else if(checkedId == R.id.radioButton2){
                    spinnerView.setEnabled(true);
                    spinnerView.setClickable(true);
                    spinnerEmployerView.setEnabled(false);
                    spinnerEmployerView.setClickable(false);
                    spinnerEmployerView.setSelection(0);
                }else{
                    spinnerView.setEnabled(false);
                    spinnerView.setClickable(false);
                    spinnerEmployerView.setEnabled(false);
                    spinnerEmployerView.setClickable(false);
                    spinnerView.setSelection(0);
                    spinnerEmployerView.setSelection(0);
                }
            }
        });
        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("users");
        editUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int getSelectLevelIndex = radioGroupView.getCheckedRadioButtonId();
                    if(getSelectLevelIndex == R.id.radioButton2 || getSelectLevelIndex == R.id.radioButton || getSelectLevelIndex == R.id.radioButton3){
                        getUserLevel = ((RadioButton)findViewById(radioGroupView.getCheckedRadioButtonId())).getText().toString();
                        if(getUserLevel.equals("Mühendis")){
                            String userUnitsValue = spinnerView.getSelectedItem().toString();
                            if(!userUnitsValue.equals("- [Birim] -")){
                                userUpdateProccess(getUserLevel,userUnitsValue,"");
                            }else{
                                Toast.makeText(UserDetailActivity.this, "Birim seçimi yapmadınız.", Toast.LENGTH_SHORT).show();
                            }
                        }else if(getUserLevel.equals("İşçi")){
                            String userShiftValue = spinnerEmployerView.getSelectedItem().toString();
                            if(!userShiftValue.equals("- [Vardiya] -")){
                                userUpdateProccess(getUserLevel,"",userShiftValue);
                            }else{
                                Toast.makeText(UserDetailActivity.this, "Vardiya seçimi yapmadınız.", Toast.LENGTH_SHORT).show();
                            }
                        }else if(getUserLevel.equals("Yönetici")){
                            userUpdateProccess(getUserLevel,"Ana Birim","");
                        }else{
                            Toast.makeText(UserDetailActivity.this, "Birim yada vardiyalardan seçim yapmadınız.", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(UserDetailActivity.this, "Üyelik türünü seçmediniz.", Toast.LENGTH_SHORT).show();
                    }
            }
        });

    }

    private void userUpdateProccess(final String getUserLevel,final String userUnitsValue,final String userShiftValue) {
        boolean changeData = false;
        if(currentUsers.getUserLevel().equals("Yönetici")){
            if(users.getSuperUser() == 1){
                Toast.makeText(this, "Süper yöneticiyi kimse düzenleyemez.", Toast.LENGTH_SHORT).show();
                changeData = false;
            }else{
                if(!getUserLevel.isEmpty()){
                    users.setUserLevel(getUserLevel);
                    users.setUnit(userUnitsValue);
                    users.setShift(userShiftValue);
                    rootRef.child(users.getUsername()).setValue(users);
                    Toast.makeText(UserDetailActivity.this, "Kullanıcı kayıdı güncellenmiştir.", Toast.LENGTH_SHORT).show();
                    changeData = true;
                }else{
                    Toast.makeText(UserDetailActivity.this, "Bir şeyler ters gitti.", Toast.LENGTH_SHORT).show();
                    changeData = false;
                }
            }
            if(currentUsers.getUsername().equals(users.getUsername()) && changeData == true){
                sp.edit().remove("cuser").commit();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finishAffinity();
                Toast.makeText(UserDetailActivity.this, "Kendi yetkileirinizi değiştirdiğinzi için oturumunuz sonlandırılmıştır.", Toast.LENGTH_LONG).show();
            }

        }else{
            Toast.makeText(this, "Sadece yöneticiler kullanıcı yetkilerini düzenleyebilirler.", Toast.LENGTH_SHORT).show();
        }


    }

    public void getSelectedRadioGroup(String param){
        switch (param){
            case "İşçi":
                radioGroupView.check(R.id.radioButton);
                spinnerView.setEnabled(false);
                spinnerView.setClickable(false);
                spinnerEmployerView.setEnabled(true);
                spinnerEmployerView.setClickable(true);
                spinnerView.setSelection(0);
                break;
            case "Mühendis":
                radioGroupView.check(R.id.radioButton2);
                spinnerView.setEnabled(true);
                spinnerView.setClickable(true);
                spinnerEmployerView.setEnabled(false);
                spinnerEmployerView.setClickable(false);
                spinnerEmployerView.setSelection(0);
                break;
            case "Yönetici":
                radioGroupView.check(R.id.radioButton3);
                spinnerView.setEnabled(false);
                spinnerView.setClickable(false);
                spinnerEmployerView.setEnabled(false);
                spinnerEmployerView.setClickable(false);
                spinnerView.setSelection(0);
                spinnerEmployerView.setSelection(0);
                break;
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int menuId = menuItem.getItemId();
        if(menuId == R.id.action_error_list){
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
        }else if(menuId == R.id.action_about){
            Toast.makeText(this, "Bu uygulama Ahmet CAN tarafından yapılmıştır.", Toast.LENGTH_SHORT).show();
        }else if(menuId == R.id.action_logout){
            sp.edit().remove("cuser").commit();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
            Toast.makeText(getApplicationContext(), "Oturumunuz kapatılmıştır.", Toast.LENGTH_LONG).show();
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return false;
    }
}
