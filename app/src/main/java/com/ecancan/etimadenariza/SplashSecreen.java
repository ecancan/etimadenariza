package com.ecancan.etimadenariza;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.ecancan.etimadenariza.Modules.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

public class SplashSecreen extends AppCompatActivity {
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private Users users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_secreen);

        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("users");
        try{
            sp = getSharedPreferences("currentUserInfo",MODE_PRIVATE);
            editor = sp.edit();
            Gson gson = new Gson();
            String json = sp.getString("cuser", "Veri Yok");
            users = gson.fromJson(json, Users.class);
            if(!users.getUsername().isEmpty() && !users.getPassword().isEmpty()){
                loginProcess(users.getUsername(), users.getPassword());
            }else{
                redirectMainActivity();
            }
        }catch (Exception e){
            redirectMainActivity();
        }

    }

    private void loginProcess(final String getUsernames,final String getPasswords) {
        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(getUsernames).exists()){
                    if(!getUsernames.isEmpty()){
                        com.ecancan.etimadenariza.Modules.Users users = dataSnapshot.child(getUsernames).getValue(Users.class);
                        if(users.getPassword().equals(getPasswords)){
                            Toast.makeText(SplashSecreen.this, "Giriş Başarılı", Toast.LENGTH_SHORT).show();
                            Gson gson = new Gson();
                            String json = gson.toJson(users);
                            editor.putString("cuser", json);
                            editor.commit();
                            FirebaseInstanceId.getInstance().getInstanceId()
                                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                            if (!task.isSuccessful()) {
                                                Log.w("Instance", "instance id getirilemedi", task.getException());
                                                return;
                                            }
                                            String token = task.getResult().getToken();
                                            tokenRegister(token,getUsernames);
                                        }
                                    });

                            Intent intent = new Intent(SplashSecreen.this,HomeActivity.class);
                            //intent.putExtra("currentUserInfo", users);
                            startActivity(intent);
                            finish();

                        }else{
                            Toast.makeText(SplashSecreen.this, "Kullanıcı adı veya şifre hatalı.", Toast.LENGTH_SHORT).show();
                            redirectMainActivity();
                        }
                    }else{
                        Toast.makeText(SplashSecreen.this, "Kullanıcı kaydınız bulunamadı.", Toast.LENGTH_SHORT).show();
                        redirectMainActivity();
                    }
                }else {
                    Toast.makeText(SplashSecreen.this, "Bir şeyler ters gitti.", Toast.LENGTH_SHORT).show();
                    redirectMainActivity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void tokenRegister(String token,String getUsername){
        rootRef.child(getUsername).child("userToken").setValue(token);
    }

    public void redirectMainActivity(){
        Intent intent = new Intent(SplashSecreen.this,MainActivity.class);
        //intent.putExtra("currentUserInfo", users);
        startActivity(intent);
        finish();
    }
}
