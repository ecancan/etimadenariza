package com.ecancan.etimadenariza.Modules;

import java.io.Serializable;

public class Users implements Serializable {
    private String userKey;
    private String userLevel;
    private String username;
    private String password;
    private String userFullName;
    private String unit;
    private String shift;
    private int superUser;
    private String userToken;


    public Users(String userKey, String userLevel, String username, String password, String userFullName, String unit, String shift, int superUser) {
        this.userKey = userKey;
        this.userLevel = userLevel;
        this.username = username;
        this.password = password;
        this.userFullName = userFullName;
        this.unit = unit;
        this.shift = shift;
        this.superUser = superUser;
    }

    public Users() {
    }

    public Users(String userKey, String userLevel, String username, String password, String userFullName) {
        this.userKey = userKey;
        this.userLevel = userLevel;
        this.username = username;
        this.password = password;
        this.userFullName = userFullName;
    }

    public Users(String userKey, String userLevel, String username, String password, String userFullName, String unit) {
        this.userKey = userKey;
        this.userLevel = userLevel;
        this.username = username;
        this.password = password;
        this.userFullName = userFullName;
        this.unit = unit;
    }

    public Users(String userKey, String userLevel, String username, String password, String userFullName, String unit, String shift) {
        this.userKey = userKey;
        this.userLevel = userLevel;
        this.username = username;
        this.password = password;
        this.userFullName = userFullName;
        this.unit = unit;
        this.shift = shift;
    }
    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(String userLevel) {
        this.userLevel = userLevel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public int getSuperUser() {
        return superUser;
    }

    public void setSuperUser(int superUser) {
        this.superUser = superUser;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
