package com.ecancan.etimadenariza.Modules;

public class ServerKey {
    private String serverKey;

    public ServerKey() {
    }

    public ServerKey(String serverKey) {
        this.serverKey = serverKey;
    }

    public String getServerKey() {
        return serverKey;
    }

    public void setServerKey(String serverKey) {
        this.serverKey = serverKey;
    }
}
