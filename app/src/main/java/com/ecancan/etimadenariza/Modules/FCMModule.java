package com.ecancan.etimadenariza.Modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FCMModule {

    @Expose
    @SerializedName("notification")
    private Notification mNotification;
    @Expose
    @SerializedName("to")
    private String mTo;

    public FCMModule(Notification mNotification, String mTo) {
        this.mNotification = mNotification;
        this.mTo = mTo;
    }

    public Notification getNotification() {
        return mNotification;
    }

    public void setNotification(Notification mNotification) {
        this.mNotification = mNotification;
    }

    public String getTo() {
        return mTo;
    }

    public void setTo(String mTo) {
        this.mTo = mTo;
    }

    public static class Notification {
        public Notification(String mTitle, String mBody) {
            this.mTitle = mTitle;
            this.mBody = mBody;
        }

        @Expose
        @SerializedName("title")
        private String mTitle;
        @Expose
        @SerializedName("body")
        private String mBody;

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(String mTitle) {
            this.mTitle = mTitle;
        }

        public String getBody() {
            return mBody;
        }

        public void setBody(String mBody) {
            this.mBody = mBody;
        }
    }
}
