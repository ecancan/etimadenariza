package com.ecancan.etimadenariza.Modules;

import java.io.Serializable;
import java.time.LocalDate;

public class Errors implements Serializable {
    private String errorKey;
    private String errorTime;
    private String errorTitle;
    private String errorDesc;
    private String errorAdmin;
    private String errorAdminDesc;
    private String errorUnit;
    private String errorShift;
    private String errorStatusByAdmin;
    private String errorEmployerDesc;
    private String errorStatusByEmployer;
    private long errorDateIntValue;
    private int errorId;

    public Errors(String errorKey, String errorTime, String errorTitle, String errorDesc, String errorAdmin, String errorAdminDesc, String errorUnit, String errorShift, String errorStatusByAdmin, String errorEmployerDesc, String errorStatusByEmployer, long errorDateIntValue, int errorId) {
        this.errorKey = errorKey;
        this.errorTime = errorTime;
        this.errorTitle = errorTitle;
        this.errorDesc = errorDesc;
        this.errorAdmin = errorAdmin;
        this.errorAdminDesc = errorAdminDesc;
        this.errorUnit = errorUnit;
        this.errorShift = errorShift;
        this.errorStatusByAdmin = errorStatusByAdmin;
        this.errorEmployerDesc = errorEmployerDesc;
        this.errorStatusByEmployer = errorStatusByEmployer;
        this.errorDateIntValue = errorDateIntValue;
        this.errorId = errorId;
    }

    public Errors(String errorKey, String errorTime, String errorTitle, String errorDesc, String errorAdmin, String errorAdminDesc, String errorUnit, String errorShift, String errorStatusByAdmin, String errorEmployerDesc, String errorStatusByEmployer, long errorDateIntValue) {
        this.errorKey = errorKey;
        this.errorTime = errorTime;
        this.errorTitle = errorTitle;
        this.errorDesc = errorDesc;
        this.errorAdmin = errorAdmin;
        this.errorAdminDesc = errorAdminDesc;
        this.errorUnit = errorUnit;
        this.errorShift = errorShift;
        this.errorStatusByAdmin = errorStatusByAdmin;
        this.errorEmployerDesc = errorEmployerDesc;
        this.errorStatusByEmployer = errorStatusByEmployer;
        this.errorDateIntValue = errorDateIntValue;
    }

    public Errors(String errorKey, String errorTime, String errorTitle, String errorDesc, String errorAdmin, String errorAdminDesc, String errorUnit, String errorShift, String errorStatusByAdmin, String errorEmployerDesc, String errorStatusByEmployer) {
        this.errorKey = errorKey;
        this.errorTime = errorTime;
        this.errorTitle = errorTitle;
        this.errorDesc = errorDesc;
        this.errorAdmin = errorAdmin;
        this.errorAdminDesc = errorAdminDesc;
        this.errorUnit = errorUnit;
        this.errorShift = errorShift;
        this.errorStatusByAdmin = errorStatusByAdmin;
        this.errorEmployerDesc = errorEmployerDesc;
        this.errorStatusByEmployer = errorStatusByEmployer;
    }

    public Errors() {
    }

    public Errors(String errorKey, String errorTime, String errorTitle, String errorDesc, String errorAdmin, String errorAdminDesc, String errorUnit, String errorShift, String errorStatusByAdmin, String errorStatusByEmployer) {
        this.errorKey = errorKey;
        this.errorTime = errorTime;
        this.errorTitle = errorTitle;
        this.errorDesc = errorDesc;
        this.errorAdmin = errorAdmin;
        this.errorAdminDesc = errorAdminDesc;
        this.errorUnit = errorUnit;
        this.errorShift = errorShift;
        this.errorStatusByAdmin = errorStatusByAdmin;
        this.errorStatusByEmployer = errorStatusByEmployer;
    }

    public Errors(String errorKey, String errorTime, String errorTitle, String errorDesc, String errorUnit) {
        this.errorKey = errorKey;
        this.errorTime = errorTime;
        this.errorTitle = errorTitle;
        this.errorDesc = errorDesc;
        this.errorUnit = errorUnit;
    }

    public Errors(String errorKey,String errorTime,String errorTitle, String errorDesc, String errorAdmin,String errorAdminDesc, String errorUnit) {
        this.errorKey = errorKey;
        this.errorTime = errorTime;
        this.errorTitle = errorTitle;
        this.errorDesc = errorDesc;
        this.errorAdmin = errorAdmin;
        this.errorAdminDesc = errorAdminDesc;
        this.errorUnit = errorUnit;
    }

    public Errors(String errorKey,String errorTime,String errorTitle, String errorDesc, String errorAdmin,String errorAdminDesc, String errorUnit, String errorShift) {
        this.errorKey = errorKey;
        this.errorTime = errorTime;
        this.errorTitle = errorTitle;
        this.errorDesc = errorDesc;
        this.errorAdmin = errorAdmin;
        this.errorAdminDesc = errorAdminDesc;
        this.errorUnit = errorUnit;
        this.errorShift = errorShift;
    }

    public Errors(String errorKey,String errorTime,String errorTitle, String errorDesc, String errorAdmin,String errorAdminDesc, String errorUnit, String errorShift, String errorStatusByAdmin) {
        this.errorKey = errorKey;
        this.errorTime = errorTime;
        this.errorTitle = errorTitle;
        this.errorDesc = errorDesc;
        this.errorAdmin = errorAdmin;
        this.errorAdminDesc = errorAdminDesc;
        this.errorUnit = errorUnit;
        this.errorShift = errorShift;
        this.errorStatusByAdmin = errorStatusByAdmin;
    }

    public long getErrorDateIntValue() {
        return errorDateIntValue;
    }

    public void setErrorDateIntValue(int errorDateIntValue) {
        this.errorDateIntValue = errorDateIntValue;
    }

    public String getErrorEmployerDesc() {
        return errorEmployerDesc;
    }

    public void setErrorEmployerDesc(String errorEmployerDesc) {
        this.errorEmployerDesc = errorEmployerDesc;
    }

    public String getErrorAdmin() {
        return errorAdmin;
    }

    public void setErrorAdmin(String errorAdmin) {
        this.errorAdmin = errorAdmin;
    }

    public String getErrorKey() {
        return errorKey;
    }

    public void setErrorKey(String errorKey) {
        this.errorKey = errorKey;
    }

    public String getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(String erorTime) {
        this.errorTime = erorTime;
    }

    public String getErrorTitle() {
        return errorTitle;
    }

    public void setErrorTitle(String errorTitle) {
        this.errorTitle = errorTitle;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getErrorAdminDesc() {
        return errorAdminDesc;
    }

    public void setErrorAdminDesc(String errorAdminDesc) {
        this.errorAdminDesc = errorAdminDesc;
    }

    public String getErrorUnit() {
        return errorUnit;
    }

    public void setErrorUnit(String errorUnit) {
        this.errorUnit = errorUnit;
    }

    public String getErrorShift() {
        return errorShift;
    }

    public void setErrorShift(String errorShift) {
        this.errorShift = errorShift;
    }

    public String getErrorStatusByAdmin() {
        return errorStatusByAdmin;
    }

    public void setErrorStatusByAdmin(String errorStatusByAdmin) {
        this.errorStatusByAdmin = errorStatusByAdmin;
    }

    public String getErrorStatusByEmployer() {
        return errorStatusByEmployer;
    }

    public void setErrorStatusByEmployer(String errorStatusByEmployer) {
        this.errorStatusByEmployer = errorStatusByEmployer;
    }

    public int getErrorId() {
        return errorId;
    }

    public void setErrorId(int errorId) {
        this.errorId = errorId;
    }
}
