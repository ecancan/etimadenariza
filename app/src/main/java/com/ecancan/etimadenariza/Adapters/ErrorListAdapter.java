package com.ecancan.etimadenariza.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.AlertDialogLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.ecancan.etimadenariza.ErrorDetailActivity;
import com.ecancan.etimadenariza.Modules.Errors;
import com.ecancan.etimadenariza.Modules.Users;
import com.ecancan.etimadenariza.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

public class ErrorListAdapter extends RecyclerView.Adapter<ErrorListAdapter.CardDesignHolder>{
    private Context mContext;
    private List<Errors> errorList;
    private PopupMenu popupMenu;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private Users currentUsers;
    private SharedPreferences sp;

    public ErrorListAdapter(Context mContext, List<Errors> errorList) {
        this.mContext = mContext;
        this.errorList = errorList;
    }



    public class CardDesignHolder extends RecyclerView.ViewHolder{
        private CardView errorCardView;
        private TextView errorName;
        private ImageView errorStatus;
        private TextView errorTime;
        private ImageView errorImageStatus;
        private ImageView errorOptionMenu;


        public CardDesignHolder(View view){
            super(view);
            errorCardView = view.findViewById(R.id.arizaCardView);
            errorName = view.findViewById(R.id.arizaName);
            errorStatus = view.findViewById(R.id.imageViewErrorStatus);
            errorTime = view.findViewById(R.id.arizaZaman);
            errorImageStatus = view.findViewById(R.id.imageViewErrorStatus);
            errorOptionMenu = view.findViewById(R.id.imageViewOptionsImage);
        }
    }


    @NonNull
    @Override
    public CardDesignHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_error_base,parent,false);

        return new CardDesignHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CardDesignHolder cardDesignHolder, int position) {
        final Errors error = errorList.get(position);
        cardDesignHolder.errorName.setText(error.getErrorTitle());
        cardDesignHolder.errorTime.setText(error.getErrorTime());
        cardDesignHolder.errorStatus.setImageResource(mContext.getResources().getIdentifier(
                error.getErrorStatusByAdmin(),
                "drawable",
                mContext.getPackageName()
        ));
        cardDesignHolder.errorName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ErrorDetailActivity.class);
                intent.putExtra("errorDetail", error);
                mContext.startActivity(intent);
            }
        });
        cardDesignHolder.errorImageStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ErrorDetailActivity.class);
                intent.putExtra("errorDetail", error);
                mContext.startActivity(intent);
            }
        });
        cardDesignHolder.errorTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ErrorDetailActivity.class);
                intent.putExtra("errorDetail", error);
                mContext.startActivity(intent);
            }
        });
        cardDesignHolder.errorOptionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu = new PopupMenu(mContext,cardDesignHolder.errorOptionMenu, Gravity.END);
                popupMenu.inflate(R.menu.error_option_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        sp = mContext.getSharedPreferences("currentUserInfo", Context.MODE_PRIVATE);
                        Gson gson = new Gson();
                        String json = sp.getString("cuser", "");
                        currentUsers = gson.fromJson(json, Users.class);

                        if(!currentUsers.getUserLevel().equals("İşçi")){
                            if(currentUsers.getUserLevel().equals("Yönetici")){
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext,AlertDialog.THEME_HOLO_LIGHT);
                                alertDialog.setTitle("Uyarı");
                                alertDialog.setMessage("\" "+error.getErrorTitle() + " \" başlıklı ariza kaydini silmek istediğinizden emin misiniz?");
                                alertDialog.setIcon(R.drawable.ic_warning_black_24dp_alert_dialog);
                                alertDialog.setPositiveButton("Sil", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        database = FirebaseDatabase.getInstance();
                                        rootRef = database.getReference("errors");
                                        rootRef.child(error.getErrorKey()).removeValue();
                                        Toast.makeText(mContext, "Ariza kaydı silinmiştir.", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                AlertDialog alert = alertDialog.create();
                                alert.show();
                            }else if(currentUsers.getUserLevel().equals("Mühendis") && error.getErrorStatusByAdmin().equals("open")){
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext,AlertDialog.THEME_HOLO_LIGHT);
                                alertDialog.setTitle("Uyarı");
                                alertDialog.setMessage("\" "+error.getErrorTitle() + " \" başlıklı ariza kaydini silmek istediğinizden emin misiniz?");
                                alertDialog.setIcon(R.drawable.ic_warning_black_24dp_alert_dialog);
                                alertDialog.setPositiveButton("Sil", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        database = FirebaseDatabase.getInstance();
                                        rootRef = database.getReference("errors");
                                        rootRef.child(error.getErrorKey()).removeValue();
                                        Toast.makeText(mContext, "Ariza kaydı silinmiştir.", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                AlertDialog alert = alertDialog.create();
                                alert.show();
                            }else{
                                Toast.makeText(mContext, "Ariza kaydını silmeniz için artık yetkiniz yoktur.", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(mContext, "Ariza kaydını silmeniz için yetkiniz yoktur.", Toast.LENGTH_SHORT).show();
                        }

                        return false;
                    }
                });
                //displaying the popup
                popupMenu.show();
            }
        });



    }

    @Override
    public int getItemCount() {
        return errorList.size();
    }
}
