package com.ecancan.etimadenariza.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.ecancan.etimadenariza.ErrorDetailActivity;
import com.ecancan.etimadenariza.Modules.Users;
import com.ecancan.etimadenariza.R;
import com.ecancan.etimadenariza.UserDetailActivity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.util.List;

public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.CardDesignHolder>{
    private Context mContext;
    private List<Users> userList;
    private PopupMenu popupMenu;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private Users currentUsers;
    private SharedPreferences sp;


    public UsersListAdapter(Context mContext, List<Users> userList) {
        this.mContext = mContext;
        this.userList = userList;
    }



    public class CardDesignHolder extends RecyclerView.ViewHolder{
        private CardView userCardView;
        private TextView userName;
        private ImageView userStatus;
        private TextView userStaff;
        private ImageView errorOptionMenu;

        public CardDesignHolder(View view){
            super(view);
            userCardView = view.findViewById(R.id.userCardView);
            userName = view.findViewById(R.id.userName);
            userStatus = view.findViewById(R.id.imageViewUser);
            userStaff = view.findViewById(R.id.userStaff);
            errorOptionMenu = view.findViewById(R.id.imageViewOptionsImage);
        }
    }


    @NonNull
    @Override
    public CardDesignHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_users_base,parent,false);

        return new CardDesignHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CardDesignHolder cardDesignHolder, int position) {
        final Users users = userList.get(position);
        cardDesignHolder.userName.setText(users.getUserFullName());
        cardDesignHolder.userStaff.setText(users.getUserLevel());
        cardDesignHolder.userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, UserDetailActivity.class);
                intent.putExtra("userDetail", users);
                mContext.startActivity(intent);
            }
        });
        cardDesignHolder.userStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, UserDetailActivity.class);
                intent.putExtra("userDetail", users);
                mContext.startActivity(intent);
            }
        });
        cardDesignHolder.userStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, UserDetailActivity.class);
                intent.putExtra("userDetail", users);
                mContext.startActivity(intent);
            }
        });

        cardDesignHolder.errorOptionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu = new PopupMenu(mContext,cardDesignHolder.errorOptionMenu, Gravity.END);
                popupMenu.inflate(R.menu.error_option_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        sp = mContext.getSharedPreferences("currentUserInfo", Context.MODE_PRIVATE);
                        Gson gson = new Gson();
                        String json = sp.getString("cuser", "");
                        currentUsers = gson.fromJson(json, Users.class);
                            if(currentUsers.getUserLevel().equals("Yönetici")){
                                if(users.getSuperUser() != 1){
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext,AlertDialog.THEME_HOLO_LIGHT);
                                    alertDialog.setTitle("Uyarı");
                                    alertDialog.setMessage("\" "+ users.getUserFullName() + " \" isimli kullanıcı kaydini silmek istediğinizden emin misiniz?");
                                    alertDialog.setIcon(R.drawable.ic_warning_black_24dp_alert_dialog);
                                    alertDialog.setPositiveButton("Sil", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            database = FirebaseDatabase.getInstance();
                                            rootRef = database.getReference("users");
                                            rootRef.child(users.getUserKey()).removeValue();
                                            Toast.makeText(mContext, "Kullanıcı kaydı silinmiştir.", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    AlertDialog alert = alertDialog.create();
                                    alert.show();
                                }else{
                                    Toast.makeText(mContext, "Süper yöneticiyi silemezsiniz.", Toast.LENGTH_SHORT).show();
                                }

                            }else{
                                Toast.makeText(mContext, "Kullanıcı kaydını silmeniz için yetkiniz yoktur.", Toast.LENGTH_SHORT).show();
                            }

                        return false;
                    }
                });
                //displaying the popup
                popupMenu.show();
            }
        });



    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}
