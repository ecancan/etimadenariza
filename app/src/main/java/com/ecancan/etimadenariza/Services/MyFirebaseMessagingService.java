package com.ecancan.etimadenariza.Services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String ntfTitle = remoteMessage.getNotification().getTitle();
        String ntfContent = remoteMessage.getNotification().getBody();
        Log.e("Bildirim","Bilridim Başlık :" +ntfTitle+" Bildirim içerik : "+ntfContent);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }
}
