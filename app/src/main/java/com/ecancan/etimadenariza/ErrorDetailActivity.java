package com.ecancan.etimadenariza;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.support.v7.widget.Toolbar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ecancan.etimadenariza.Fragments.ErrorAddFragment;
import com.ecancan.etimadenariza.Interfaces.FCMInterface;
import com.ecancan.etimadenariza.Modules.Errors;
import com.ecancan.etimadenariza.Modules.FCMModule;
import com.ecancan.etimadenariza.Modules.ServerKey;
import com.ecancan.etimadenariza.Modules.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ErrorDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private Button button;
    private LinearLayout parentLinearLayout;
    private LayoutInflater inflater;
    private Users currentUser;
    private SharedPreferences sp;
    private Toolbar toolbar;
    private Errors errors;
    private TextView errorTitle;
    private TextView errorDesc;
    private TextView errorUnits;
    private EditText errorAdminDesc;
    private String errorAdminDescText;
    private String errorShift;
    private RadioGroup radioGroupView;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private TextView adminDescText;
    private TextView errorAdmin;
    private EditText errorEmployerDesc;
    private TextView errorEmployerDescText;
    private TextView errorShiftView;
    private RelativeLayout adminViewTextContainer;
    private RelativeLayout employerViewTextContainer;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private TextView userFullNameText;
    private TextView userLevetText;
    private DatabaseReference rootRefNotifications;

    private ServerKey serverKey;
    private String baseUrl = "https://fcm.googleapis.com/fcm/";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_detail);
        Intent i = getIntent();
        errors = (Errors) i.getSerializableExtra("errorDetail");

        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("errors");
        rootRefNotifications = database.getReference("notifications");

        toolbar = findViewById(R.id.errorDetailToolbar);
        if(errors.getErrorStatusByAdmin().equals("open")){
            toolbar.setTitle("Arıza Durumu : Açık" );
            toolbar.setBackgroundColor((Color.parseColor("#d50000")));
            statusBarChanging("#d50000");
        }else if(errors.getErrorStatusByAdmin().equals("employment")){
            toolbar.setTitle("Arıza Durumu : Görevlendirildi" );
            toolbar.setBackgroundColor((Color.parseColor("#000876")));
            statusBarChanging("#000876");
        }else if(errors.getErrorStatusByAdmin().equals("wait")){
            toolbar.setTitle("Arıza Durumu : İşlemde" );
            toolbar.setBackgroundColor((Color.parseColor("#FF8F00")));
            statusBarChanging("#FF8F00");
        }else if(errors.getErrorStatusByAdmin().equals("close")){
            toolbar.setTitle("Arıza Durumu : Kapalı" );
            toolbar.setBackgroundColor((Color.parseColor("#4CAF50")));
            statusBarChanging("#4CAF50");
        }
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        setSupportActionBar(toolbar);

        sp = getSharedPreferences("currentUserInfo",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("cuser", "");
        currentUser = gson.fromJson(json, Users.class);


        navigationView = findViewById(R.id.navigationView);
        drawerLayout = findViewById(R.id.drawerLayout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,0,0);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        View slideHeder = navigationView.inflateHeaderView(R.layout.slider_menu_header);
        navigationView.setNavigationItemSelectedListener(this);
        userFullNameText = slideHeder.findViewById(R.id.header_user_full_name);
        userLevetText = slideHeder.findViewById(R.id.header_user_level);

        userFullNameText.setText(currentUser.getUserFullName());
        userLevetText.setText(currentUser.getUserLevel());


        button = findViewById(R.id.submitButton);
        parentLinearLayout = (LinearLayout) findViewById(R.id.parentRelativeLayout);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(currentUser.getUserLevel().equals("Yönetici")){

            final View adminView = inflater.inflate(R.layout.field_admin_review, null);
            final View adminViewText = inflater.inflate(R.layout.field_admin_review_text, null);
            final View employerViewText = inflater.inflate(R.layout.field_employer_review_text, null);

            adminViewTextContainer = adminViewText.findViewById(R.id.errorContainerRelative2);
            adminViewTextContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(errors.getErrorStatusByAdmin().equals("employment")){
                        button.setEnabled(true);
                        button.setClickable(true);
                        button.setText("Görevlendir");
                        parentLinearLayout.removeView(adminViewText);
                        parentLinearLayout.addView(adminView,parentLinearLayout.getChildCount());
                        errorAdminDesc = adminView.findViewById(R.id.errorAdminDescEditText);
                        radioGroupView = adminView.findViewById(R.id.errorShiftGroup);
                        errorAdminDesc.setText(errors.getErrorAdminDesc());
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try{

                                    errorShift = ((RadioButton) adminView.findViewById(radioGroupView.getCheckedRadioButtonId())).getText().toString();
                                    errorAdminDescText = errorAdminDesc.getText().toString();
                                    if(!errorAdminDescText.isEmpty()){
                                        errors.setErrorAdminDesc(errorAdminDescText);
                                        errors.setErrorShift(errorShift);
                                        errors.setErrorStatusByAdmin("employment");
                                        errors.setErrorAdmin(currentUser.getUserFullName());
                                        rootRef.child(errors.getErrorKey()).setValue(errors, new DatabaseReference.CompletionListener(){
                                            @Override
                                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                                sendNotification("İşçi","[Yeni Görev] "+errors.getErrorTitle(),errors.getErrorAdmin() +" tarafından bu arıza için görevlendirildiniz.",errors.getErrorShift(),"");
                                            }
                                        });
                                        parentLinearLayout.removeView(adminView);
                                        toolbar.setTitle("Arıza Durumu : Görevlendirildi" );
                                        toolbar.setBackgroundColor((Color.parseColor("#000876")));
                                        statusBarChanging("#000876");

                                        adminDescText = adminViewText.findViewById(R.id.errorTitleDescDetailView);
                                        errorAdmin = adminViewText.findViewById(R.id.errorAdminView);
                                        adminDescText.setText(errors.getErrorAdminDesc());
                                        errorAdmin.setText(errors.getErrorAdmin());
                                        parentLinearLayout.addView(adminViewText, parentLinearLayout.getChildCount());
                                        button.setEnabled(false);
                                        button.setClickable(false);
                                        button.setText("Görevlendirildi");
                                    }else{
                                        Toast.makeText(ErrorDetailActivity.this, "Lütfen eksiksiz bir şekilde formu doldurun.", Toast.LENGTH_SHORT).show();
                                    }

                                }catch (Exception e){
                                    Toast.makeText(ErrorDetailActivity.this, "Lütfen eksiksiz bir şekilde formu doldurun.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                    return false;
                }
            });
            if(errors.getErrorAdminDesc().isEmpty() && errors.getErrorShift().isEmpty()){
                engineerViewMethod();

                parentLinearLayout.addView(adminView, parentLinearLayout.getChildCount());
                button.setText("Görevlendir");

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try{
                            errorAdminDesc = adminView.findViewById(R.id.errorAdminDescEditText);
                            radioGroupView = adminView.findViewById(R.id.errorShiftGroup);
                            errorShift = ((RadioButton) adminView.findViewById(radioGroupView.getCheckedRadioButtonId())).getText().toString();
                            errorAdminDescText = errorAdminDesc.getText().toString();
                            if(!errorAdminDescText.isEmpty()){
                                errors.setErrorAdminDesc(errorAdminDescText);
                                errors.setErrorShift(errorShift);
                                errors.setErrorStatusByAdmin("employment");
                                errors.setErrorAdmin(currentUser.getUserFullName());
                                rootRef.child(errors.getErrorKey()).setValue(errors, new DatabaseReference.CompletionListener(){
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                        sendNotification("İşçi","[Yeni Görev] "+errors.getErrorTitle(),errors.getErrorAdmin() +" tarafından bu arıza için görevlendirildiniz.",errors.getErrorShift(),"");
                                    }
                                });

                                parentLinearLayout.removeView(adminView);
                                toolbar.setTitle("Arıza Durumu : Görevlendirildi" );
                                toolbar.setBackgroundColor((Color.parseColor("#000876")));
                                statusBarChanging("#000876");

                                adminDescText = adminViewText.findViewById(R.id.errorTitleDescDetailView);
                                errorAdmin = adminViewText.findViewById(R.id.errorAdminView);
                                adminDescText.setText(errors.getErrorAdminDesc());
                                errorAdmin.setText(errors.getErrorAdmin());
                                parentLinearLayout.addView(adminViewText, parentLinearLayout.getChildCount());
                                button.setEnabled(false);
                                button.setClickable(false);
                                button.setText("Görevlendirildi");
                            }else{
                                Toast.makeText(ErrorDetailActivity.this, "Lütfen eksiksiz bir şekilde formu doldurun.", Toast.LENGTH_SHORT).show();
                            }

                        }catch (Exception e){
                            Toast.makeText(ErrorDetailActivity.this, "Lütfen eksiksiz bir şekilde formu doldurun.", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }else{
                engineerViewMethod();

                adminDescText = adminViewText.findViewById(R.id.errorTitleDescDetailView);
                errorAdmin = adminViewText.findViewById(R.id.errorAdminView);
                adminDescText.setText(errors.getErrorAdminDesc());
                errorAdmin.setText(errors.getErrorAdmin());
                parentLinearLayout.addView(adminViewText, parentLinearLayout.getChildCount());
                button.setEnabled(false);
                button.setClickable(false);
                button.setText("Görevlendirildi");


                try{
                    if(!errors.getErrorEmployerDesc().isEmpty()){
                        if(errors.getErrorStatusByAdmin().equals("close")){
                            button.setEnabled(false);
                            button.setClickable(false);
                            button.setText("Onaylandı");
                        }else{
                            button.setEnabled(true);
                            button.setClickable(true);
                            button.setText("Onayla");
                        }


                        errorEmployerDescText = employerViewText.findViewById(R.id.errorEmoployerDescDetailView);
                        errorShiftView = employerViewText.findViewById(R.id.errorShiftView);
                        errorEmployerDescText.setText(errors.getErrorEmployerDesc());
                        errorShiftView.setText(errors.getErrorShift());
                        parentLinearLayout.addView(employerViewText, parentLinearLayout.getChildCount());
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                errors.setErrorStatusByAdmin("close");
                                rootRef.child(errors.getErrorKey()).setValue(errors, new DatabaseReference.CompletionListener(){
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                        sendNotification("İşçi","[Arıza Çözüldü] "+errors.getErrorTitle(),errors.getErrorAdmin() +" tarafından bu arıza çözümü için onay verildi.",errors.getErrorShift(),"");
                                        sendNotification("Mühendis","[Arıza Çözüldü] "+errors.getErrorTitle(),errors.getErrorAdmin() +" tarafından bu arıza çözümü için onay verildi.","",errors.getErrorUnit());
                                    }
                                });
                                button.setEnabled(false);
                                button.setClickable(false);
                                button.setText("Onaylandı");
                                toolbar.setTitle("Arıza Durumu : Kapalı" );
                                toolbar.setBackgroundColor((Color.parseColor("#4CAF50")));
                                statusBarChanging("#4CAF50");
                            }
                        });
                    }else{
                        button.setEnabled(false);
                        button.setClickable(false);
                        button.setText("Görevlendirildi");
                    }
                }catch (Exception e){

                }

            }

        }else if(currentUser.getUserLevel().equals("Mühendis")){
            engineerViewMethod();
            button.setVisibility(View.INVISIBLE);
        }else if(currentUser.getUserLevel().equals("İşçi")){
            final View adminViewText = inflater.inflate(R.layout.field_admin_review_text, null);
            final View employerView = inflater.inflate(R.layout.field_employer_review, null);
            final View employerViewText = inflater.inflate(R.layout.field_employer_review_text, null);

            adminDescText = adminViewText.findViewById(R.id.errorTitleDescDetailView);
            errorAdmin = adminViewText.findViewById(R.id.errorAdminView);
            adminDescText.setText(errors.getErrorAdminDesc());
            errorAdmin.setText(errors.getErrorAdmin());
            parentLinearLayout.addView(adminViewText, parentLinearLayout.getChildCount());
            if(errors.getErrorStatusByAdmin().equals("wait")){
                employerViewTextContainer = employerViewText.findViewById(R.id.errorContainerRelative2);
                employerViewTextContainer.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        errorEmployerDesc = employerView.findViewById(R.id.errorEmployerDescEditText);
                        errorEmployerDesc.setText(errors.getErrorEmployerDesc());
                        parentLinearLayout.removeView(employerViewText);
                        parentLinearLayout.addView(employerView, parentLinearLayout.getChildCount());
                        button.setEnabled(true);
                        button.setClickable(true);
                        button.setText("Tamamla");
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                errors.setErrorEmployerDesc(errorEmployerDesc.getText().toString());
                                //errors.setErrorStatusByEmployer("close");
                                errors.setErrorStatusByAdmin("wait");
                                rootRef.child(errors.getErrorKey()).setValue(errors);
                                button.setText("Onay için gönderildi");
                                button.setEnabled(false);
                                button.setClickable(false);
                                parentLinearLayout.removeView(employerView);
                                toolbar.setTitle("Arıza Durumu : İşlemde" );
                                toolbar.setBackgroundColor((Color.parseColor("#FF8F00")));
                                statusBarChanging("#FF8F00");

                                errorEmployerDescText = employerViewText.findViewById(R.id.errorEmoployerDescDetailView);
                                errorShiftView = employerViewText.findViewById(R.id.errorShiftView);
                                errorEmployerDescText.setText(errors.getErrorEmployerDesc());
                                errorShiftView.setText(errors.getErrorShift());
                                parentLinearLayout.addView(employerViewText, parentLinearLayout.getChildCount());
                            }
                        });
                        return false;
                    }
                });
            }
            if(errors.getErrorStatusByAdmin().equals("employment")){
                errorEmployerDesc = employerView.findViewById(R.id.errorEmployerDescEditText);
                parentLinearLayout.addView(employerView, parentLinearLayout.getChildCount());
                button.setEnabled(true);
                button.setClickable(true);
                button.setText("Tamamla");
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        errors.setErrorEmployerDesc(errorEmployerDesc.getText().toString());
                        //errors.setErrorStatusByEmployer("close");
                        errors.setErrorStatusByAdmin("wait");
                        rootRef.child(errors.getErrorKey()).setValue(errors, new DatabaseReference.CompletionListener(){
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                sendNotification("Yönetici","[Arıza Kontrolü] "+errors.getErrorTitle(),errors.getErrorShift() +" tarafından bu arıza çözümü için onay beklenmekte.","","");
                            }
                        });
                        button.setText("Onay için gönderildi");
                        button.setEnabled(false);
                        button.setClickable(false);
                        parentLinearLayout.removeView(employerView);
                        toolbar.setTitle("Arıza Durumu : İşlemde" );
                        toolbar.setBackgroundColor((Color.parseColor("#FF8F00")));
                        statusBarChanging("#FF8F00");

                        errorEmployerDescText = employerViewText.findViewById(R.id.errorEmoployerDescDetailView);
                        errorShiftView = employerViewText.findViewById(R.id.errorShiftView);
                        errorEmployerDescText.setText(errors.getErrorEmployerDesc());
                        errorShiftView.setText(errors.getErrorShift());
                        parentLinearLayout.addView(employerViewText, parentLinearLayout.getChildCount());
                    }
                });
            }else{

                errorEmployerDescText = employerViewText.findViewById(R.id.errorEmoployerDescDetailView);
                errorShiftView = employerViewText.findViewById(R.id.errorShiftView);
                errorEmployerDescText.setText(errors.getErrorEmployerDesc());
                errorShiftView.setText(errors.getErrorShift());
                parentLinearLayout.addView(employerViewText, parentLinearLayout.getChildCount());


                if(errors.getErrorStatusByAdmin().equals("close")){
                    button.setText("Onaylandı");
                }else{
                    button.setText("Onay için gönderildi");
                }
                button.setEnabled(false);
                button.setClickable(false);
            }
        }


    }
    private void statusBarChanging(String hexColor){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getDarkColor(Color.parseColor(hexColor), 1));
        }
    }
    private int getDarkColor(int color, double value) {
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        return Color.rgb((int) (r * value), (int) (g * value), (int) (b * value));
    }

    public void engineerViewMethod(){
        final View engineerView = inflater.inflate(R.layout.field_engineer_review, null);
        errorTitle = engineerView.findViewById(R.id.errorTitleDetailView);
        errorDesc = engineerView.findViewById(R.id.errorTitleDescDetailView);
        errorUnits = engineerView.findViewById(R.id.errorTitleUnitView);
        errorTitle.setText(errors.getErrorTitle());
        errorDesc.setText(errors.getErrorDesc());
        errorUnits.setText(errors.getErrorUnit());
        parentLinearLayout.addView(engineerView, parentLinearLayout.getChildCount());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            sp.edit().remove("cuser").commit();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
            Toast.makeText(ErrorDetailActivity.this, "Oturumunuz kapatılmıştır.", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int menuId = menuItem.getItemId();
        if(menuId == R.id.action_error_list){
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
        }else if(menuId == R.id.action_about){
            Toast.makeText(this, "Bu uygulama Ahmet CAN tarafından yapılmıştır.", Toast.LENGTH_SHORT).show();
        }else if(menuId == R.id.action_logout){
            sp.edit().remove("cuser").commit();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
            Toast.makeText(ErrorDetailActivity.this, "Oturumunuz kapatılmıştır.", Toast.LENGTH_LONG).show();
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    public void sendNotification(final String userLevel, final String notificationTitle, final String notificationBody,final String errorShiftText,final String errorUnitText){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference rootRefUsers = database.getReference("users");
        Query query = rootRefUsers.orderByChild("userLevel").equalTo(userLevel);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot d : dataSnapshot.getChildren()){
                    final Users users = d.getValue(Users.class);
                    if(userLevel.equals("İşçi")){
                        if(users.getShift().equals(errorShiftText)){
                            getServerReader(new ErrorDetailActivity.serverCallback() {
                                @Override
                                public void onCallback(String value) {
                                    Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
                                    FCMInterface fcmInterface = retrofit.create(FCMInterface.class);
                                    HashMap<String,String> headers = new HashMap<String, String>();
                                    headers.put("Content-Type","application/json");
                                    headers.put("Authorization","key="+value);
                                    final FCMModule.Notification notification = new FCMModule.Notification(notificationTitle,notificationBody);
                                    FCMModule fcmModule = new FCMModule(notification,users.getUserToken());
                                    fcmInterface.notifications(headers,fcmModule).enqueue(new Callback<Response<FCMModule>>() {
                                        @Override
                                        public void onResponse(Call<Response<FCMModule>> call, Response<Response<FCMModule>> response) {
                                            Log.e("retrofit","success");
                                        }

                                        @Override
                                        public void onFailure(Call<Response<FCMModule>> call, Throwable t) {
                                            Log.e("retrofit","failed");
                                        }
                                    });
                                }
                            });
                        }
                    }else if(userLevel.equals("Mühendis")){
                        if(users.getUnit().equals(errorUnitText)){
                            getServerReader(new ErrorDetailActivity.serverCallback() {
                                @Override
                                public void onCallback(String value) {
                                    Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
                                    FCMInterface fcmInterface = retrofit.create(FCMInterface.class);
                                    HashMap<String,String> headers = new HashMap<String, String>();
                                    headers.put("Content-Type","application/json");
                                    headers.put("Authorization","key="+value);
                                    final FCMModule.Notification notification = new FCMModule.Notification(notificationTitle,notificationBody);
                                    FCMModule fcmModule = new FCMModule(notification,users.getUserToken());
                                    fcmInterface.notifications(headers,fcmModule).enqueue(new Callback<Response<FCMModule>>() {
                                        @Override
                                        public void onResponse(Call<Response<FCMModule>> call, Response<Response<FCMModule>> response) {
                                            Log.e("retrofit","success");
                                        }

                                        @Override
                                        public void onFailure(Call<Response<FCMModule>> call, Throwable t) {
                                            Log.e("retrofit","failed");
                                        }
                                    });
                                }
                            });
                        }

                    }else if(userLevel.equals("Yönetici")){
                        getServerReader(new ErrorDetailActivity.serverCallback() {
                            @Override
                            public void onCallback(String value) {
                                Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
                                FCMInterface fcmInterface = retrofit.create(FCMInterface.class);
                                HashMap<String,String> headers = new HashMap<String, String>();
                                headers.put("Content-Type","application/json");
                                headers.put("Authorization","key="+value);
                                final FCMModule.Notification notification = new FCMModule.Notification(notificationTitle,notificationBody);
                                FCMModule fcmModule = new FCMModule(notification,users.getUserToken());
                                fcmInterface.notifications(headers,fcmModule).enqueue(new Callback<Response<FCMModule>>() {
                                    @Override
                                    public void onResponse(Call<Response<FCMModule>> call, Response<Response<FCMModule>> response) {
                                        Log.e("retrofit","success");
                                    }

                                    @Override
                                    public void onFailure(Call<Response<FCMModule>> call, Throwable t) {
                                        Log.e("retrofit","failed");
                                    }
                                });
                            }
                        });
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getServerReader(@NonNull final ErrorDetailActivity.serverCallback callback){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference rootRefServer = database.getReference("server");
        rootRefServer.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                serverKey = dataSnapshot.getValue(ServerKey.class);
                callback.onCallback(serverKey.getServerKey());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface serverCallback{
        void onCallback(String value);
    }
}
