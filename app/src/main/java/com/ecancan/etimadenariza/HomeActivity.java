package com.ecancan.etimadenariza;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.app.*;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ecancan.etimadenariza.Adapters.ErrorListAdapter;
import com.ecancan.etimadenariza.Fragments.ErrorAddFragment;
import com.ecancan.etimadenariza.Fragments.ErrorFragment;
import com.ecancan.etimadenariza.Fragments.SearchErrorFragment;
import com.ecancan.etimadenariza.Fragments.SignFragment;
import com.ecancan.etimadenariza.Fragments.UsersFragment;
import com.ecancan.etimadenariza.Modules.Users;
import com.google.gson.Gson;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private Users users;
    private Toolbar toolbar;
    private BottomNavigationView bottomNav;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private TextView userFullNameText;
    private TextView userLevetText;
    private View slideHeader;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //Intent i = getIntent();
        //users = (Users) i.getSerializableExtra("currentUserInfo");
        sp = getSharedPreferences("currentUserInfo",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("cuser", "");
        users = gson.fromJson(json, Users.class);

        ErrorFragment errorFragment = new ErrorFragment();
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragmentHolder,errorFragment,"ErrorFragment");
        transaction.commit();

        bottomNav = findViewById(R.id.bottomNavMenu);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Etimaden Ariza Otomasyonu");
        //toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        setSupportActionBar(toolbar);

        navigationView = (NavigationView) findViewById(R.id.navigationView);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        navigationView.getMenu().removeItem(R.id.action_error_list);



        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,0,0);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);

        slideHeader = (View) navigationView.inflateHeaderView(R.layout.slider_menu_header);
        userFullNameText = (TextView) slideHeader.findViewById(R.id.header_user_full_name);
        userLevetText = (TextView) slideHeader.findViewById(R.id.header_user_level);

        userFullNameText.setText(users.getUserFullName());
        userLevetText.setText(users.getUserLevel());


        if(users.getUserLevel().equals("Yönetici")){
            bottomNav.getMenu().removeItem(R.id.action_error_add);
        }else if(users.getUserLevel().equals("Mühendis")){
            bottomNav.getMenu().removeItem(R.id.action_add_user);
            bottomNav.getMenu().removeItem(R.id.action_users_list);

        }else if(users.getUserLevel().equals("İşçi")){
            bottomNav.getMenu().removeItem(R.id.action_add_user);
            bottomNav.getMenu().removeItem(R.id.action_users_list);
            bottomNav.getMenu().removeItem(R.id.action_error_add);
        }

        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                if(item.getItemId() == R.id.action_error_list){
                    fragment = new ErrorFragment();
                }else if(item.getItemId() == R.id.action_error_add){
                    fragment = new ErrorAddFragment();
                }else if(item.getItemId() == R.id.action_search){
                    fragment = new SearchErrorFragment();
                }else if(item.getItemId() == R.id.action_add_user){
                    fragment = new SignFragment();
                }else if(item.getItemId() == R.id.action_users_list){
                    fragment = new UsersFragment();
                }

                FragmentManager manager = getFragmentManager();
                //fragment.setArguments();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.fragmentHolder,fragment,"ErrorFragment");
                transaction.commit();
                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            sp.edit().remove("cuser").commit();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
            Toast.makeText(HomeActivity.this, "Oturumunuz kapatılmıştır.", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int menuId = menuItem.getItemId();

        if(menuId == R.id.action_about){
            Toast.makeText(this, "Bu uygulama Ahmet CAN tarafından yapılmıştır.", Toast.LENGTH_SHORT).show();
        }else if(menuId == R.id.action_logout){
            sp.edit().remove("cuser").commit();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
            Toast.makeText(HomeActivity.this, "Oturumunuz kapatılmıştır.", Toast.LENGTH_LONG).show();
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }
}
