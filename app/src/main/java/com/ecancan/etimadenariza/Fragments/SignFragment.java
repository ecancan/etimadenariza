package com.ecancan.etimadenariza.Fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.ecancan.etimadenariza.Modules.Hash;
import com.ecancan.etimadenariza.Modules.Users;
import com.ecancan.etimadenariza.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignFragment extends Fragment {
    private Button signButtonView;
    private EditText fullNameView;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private Spinner spinnerView;
    private Spinner spinnerEmployerView;
    private RadioGroup radioGroupView;
    private RadioButton radioButtonView1;
    private RadioButton radioButtonView2;
    private RadioButton radioButtonView3;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private String getUsername;
    private String getPassword;
    private String getFullName;
    private String getUserLevel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_sign, container, false);
        signButtonView = view.findViewById(R.id.signButtonView);
        fullNameView = view.findViewById(R.id.userFullName);
        usernameEditText = view.findViewById(R.id.usernameEditText);
        passwordEditText = view.findViewById(R.id.passwordEditText);
        spinnerView = view.findViewById(R.id.spinnerSign);
        spinnerEmployerView = view.findViewById(R.id.spinnerSignEmployer);
        radioGroupView = view.findViewById(R.id.radioGroupContainer);
        radioButtonView1 = view.findViewById(R.id.radioButton);
        radioButtonView2 = view.findViewById(R.id.radioButton2);
        radioButtonView3 = view.findViewById(R.id.radioButton3);
        ArrayAdapter<String> userUnitAdapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.userUnits));
        userUnitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerView.setAdapter(userUnitAdapter);
        ArrayAdapter<String> employerAdapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.employers));
        employerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEmployerView.setAdapter(employerAdapter);
        spinnerView.setEnabled(false);
        spinnerView.setClickable(false);
        spinnerEmployerView.setEnabled(false);
        spinnerEmployerView.setClickable(false);
        radioGroupView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.radioButton){
                    spinnerView.setEnabled(false);
                    spinnerView.setClickable(false);
                    spinnerEmployerView.setEnabled(true);
                    spinnerEmployerView.setClickable(true);
                    spinnerView.setSelection(0);
                }else if(checkedId == R.id.radioButton2){
                    spinnerView.setEnabled(true);
                    spinnerView.setClickable(true);
                    spinnerEmployerView.setEnabled(false);
                    spinnerEmployerView.setClickable(false);
                    spinnerEmployerView.setSelection(0);
                }else{
                    spinnerView.setEnabled(false);
                    spinnerView.setClickable(false);
                    spinnerEmployerView.setEnabled(false);
                    spinnerEmployerView.setClickable(false);
                    spinnerView.setSelection(0);
                    spinnerEmployerView.setSelection(0);
                }
            }
        });


        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("users");
        signButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                getUsername = usernameEditText.getText().toString().toLowerCase();
                getPassword = passwordEditText.getText().toString();
                getFullName = fullNameView.getText().toString();
                int getSelectLevelIndex = radioGroupView.getCheckedRadioButtonId();
                if(!getUsername.isEmpty() && !getPassword.isEmpty() && !getFullName.isEmpty()){
                    if(getSelectLevelIndex == R.id.radioButton2 || getSelectLevelIndex == R.id.radioButton || getSelectLevelIndex == R.id.radioButton3){
                        getUserLevel = ((RadioButton) view.findViewById(radioGroupView.getCheckedRadioButtonId())).getText().toString();
                        if(getUserLevel.equals("Mühendis")){
                            String userUnitsValue = spinnerView.getSelectedItem().toString();
                            if(!userUnitsValue.equals("- [Birim] -")){
                                signProccess(getUsername, Hash.md5(getPassword),getFullName,getUserLevel,userUnitsValue,"");
                                clearForm();
                            }else{
                                Toast.makeText(getActivity(), "Birim seçimi yapmadınız.", Toast.LENGTH_SHORT).show();
                            }
                        }else if(getUserLevel.equals("İşçi")){
                            String userShiftValue = spinnerEmployerView.getSelectedItem().toString();
                            if(!userShiftValue.equals("- [Vardiya] -")){
                                signProccess(getUsername,Hash.md5(getPassword),getFullName,getUserLevel,"",userShiftValue);
                                clearForm();
                            }else{
                                Toast.makeText(getActivity(), "Vardiya seçimi yapmadınız.", Toast.LENGTH_SHORT).show();
                            }
                        }else if(getUserLevel.equals("Yönetici")){
                            signProccess(getUsername,Hash.md5(getPassword),getFullName,getUserLevel,"Ana Birim","");
                            clearForm();
                        }else{
                            Toast.makeText(getActivity(), "Birim yada vardiyalardan seçim yapmadınız.", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(getActivity(), "Üyelik türünü seçmediniz.", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Eksik bilgi mevcut.", Toast.LENGTH_SHORT).show();
                }



            }
        });

        return view;
    }
    private void signProccess(final String getUsername, final String getPassword,final String getFullName,final String getUserLevel,final String userUnitsValue,final String userShiftValue) {
        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(getUsername).exists()){
                    if(!getUsername.isEmpty()){
                        Users users = dataSnapshot.child(getUsername).getValue(Users.class);
                        if(users.getUsername().equals(getUsername)){
                            Toast.makeText(getActivity(), "Bu kullanıcı adı alınmış.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else{
                    Users users = new Users("",getUserLevel,getUsername,getPassword,getFullName,userUnitsValue,userShiftValue);
                    rootRef.child(users.getUsername()).setValue(users);
                    Toast.makeText(getActivity(), "Kullanıcı kayıdı yapılmıştır.", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void clearForm(){
        usernameEditText.setText("");
        passwordEditText.setText("");
        fullNameView.setText("");
        radioGroupView.clearCheck();
        spinnerView.setEnabled(false);
        spinnerView.setClickable(false);
        spinnerEmployerView.setEnabled(false);
        spinnerEmployerView.setClickable(false);
        spinnerView.setSelection(0);
        spinnerEmployerView.setSelection(0);
    }
}


