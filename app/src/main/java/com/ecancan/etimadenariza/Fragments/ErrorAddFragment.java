package com.ecancan.etimadenariza.Fragments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ecancan.etimadenariza.Interfaces.FCMInterface;
import com.ecancan.etimadenariza.Modules.Errors;
import com.ecancan.etimadenariza.Modules.FCMModule;
import com.ecancan.etimadenariza.Modules.ServerKey;
import com.ecancan.etimadenariza.Modules.Users;
import com.ecancan.etimadenariza.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ErrorAddFragment extends Fragment {
    private EditText errorTitle;
    private EditText errorDesc;
    private Users currentUsers;
    private Button sendError;
    private SimpleDateFormat dateFormat;
    private String localDateString;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private String currentUsersUnit;
    private SharedPreferences sp;
    private int checkState = 1;
    private Errors errorAdd;

    private ServerKey serverKey;
    private String baseUrl = "https://fcm.googleapis.com/fcm/";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        sp = getActivity().getSharedPreferences("currentUserInfo", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("cuser", "");
        currentUsers = gson.fromJson(json, Users.class);
        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("errors");



    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_error_add, container, false);


        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        localDateString = dateFormat.format(new Date());

        errorTitle = view.findViewById(R.id.errorTitleEditText);
        errorDesc = view.findViewById(R.id.errorDescEditText);
        sendError = view.findViewById(R.id.sendErrorButton);

        currentUsersUnit = currentUsers.getUnit();

        sendError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendErrorDB(errorTitle.getText().toString(),errorDesc.getText().toString(),currentUsersUnit,localDateString);
            }
        });



        return view;


    }
    public void sendErrorDB(final String getErrorTitle,final String getErrorDesc,final String getUserUnit,final String getErrorDate){
        if(!getErrorTitle.isEmpty() && !getErrorDesc.isEmpty() && !getUserUnit.isEmpty() && !getErrorDate.isEmpty()){
            checkState = 1;
            Query query = rootRef.orderByChild("errorDateIntValue").limitToLast(1);
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot){
                        for(DataSnapshot d: dataSnapshot.getChildren()){
                            if(checkState == 1){
                                final Errors getErrors = d.getValue(Errors.class);
                                errorAdd = new Errors("",getErrorDate,getErrorTitle,getErrorDesc,"","",getUserUnit,"","open","","",(long) (new Date().getTime()/1000),getErrors.getErrorId() + 1);
                                rootRef.push().setValue(errorAdd, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                        sendNotification("Yönetici","[Yeni Arıza] "+ errorAdd.getErrorTitle(),errorAdd.getErrorUnit()+" tarafından arıza kaydı oluşturuldu.");
                                    }
                                });
                                checkState++;
                            }

                        }
                        if(checkState == 1){
                            errorAdd = new Errors("",getErrorDate,getErrorTitle,getErrorDesc,"","",getUserUnit,"","open","","",(long) (new Date().getTime()/1000),0);
                            rootRef.push().setValue(errorAdd, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                    sendNotification("Yönetici","[Yeni Arıza] "+ errorAdd.getErrorTitle(),errorAdd.getErrorUnit()+" tarafından arıza kaydı oluşturuldu.");
                                }
                            });
                            checkState++;
                        }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


            Toast.makeText(getActivity(), "Arıza kaydı gönderilmiştir.", Toast.LENGTH_SHORT).show();
            clearForm();
            ErrorFragment errorFragment = new ErrorFragment();
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.fragmentHolder,errorFragment,"ErrorFragment");
            transaction.commit();

        }else{
            Toast.makeText(getActivity(), "Arıza kaydını eksiksiz doldurun.", Toast.LENGTH_SHORT).show();
        }
    }
    public void clearForm(){
        errorTitle.setText("");
        errorDesc.setText("");
    }

    public void sendNotification(String userLevel, final String notificationTitle, final String notificationBody){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference rootRefUsers = database.getReference("users");
        Query query = rootRefUsers.orderByChild("userLevel").equalTo(userLevel);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot d : dataSnapshot.getChildren()){
                    final Users users = d.getValue(Users.class);

                    getServerReader(new serverCallback() {
                        @Override
                        public void onCallback(String value) {
                            Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
                            FCMInterface fcmInterface = retrofit.create(FCMInterface.class);
                            HashMap<String,String> headers = new HashMap<String, String>();
                            headers.put("Content-Type","application/json");
                            headers.put("Authorization","key="+value);
                            System.out.println(value);
                            final FCMModule.Notification notification = new FCMModule.Notification(notificationTitle,notificationBody);
                            FCMModule fcmModule = new FCMModule(notification,users.getUserToken());
                            fcmInterface.notifications(headers,fcmModule).enqueue(new Callback<Response<FCMModule>>() {
                                @Override
                                public void onResponse(Call<Response<FCMModule>> call, Response<Response<FCMModule>> response) {
                                    Log.e("retrofit","success");
                                }

                                @Override
                                public void onFailure(Call<Response<FCMModule>> call, Throwable t) {
                                    Log.e("retrofit","failed");
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getServerReader(@NonNull final serverCallback callback){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference rootRefServer = database.getReference("server");
        rootRefServer.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                serverKey = dataSnapshot.getValue(ServerKey.class);
                callback.onCallback(serverKey.getServerKey());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface serverCallback{
        void onCallback(String value);
    }

}
