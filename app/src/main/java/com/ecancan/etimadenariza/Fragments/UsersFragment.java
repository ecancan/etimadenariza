package com.ecancan.etimadenariza.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ecancan.etimadenariza.Adapters.ErrorListAdapter;
import com.ecancan.etimadenariza.Adapters.UsersListAdapter;
import com.ecancan.etimadenariza.Modules.Errors;
import com.ecancan.etimadenariza.Modules.Users;
import com.ecancan.etimadenariza.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;

public class UsersFragment extends Fragment {
    private RecyclerView rv;
    private ArrayList<Users> usersArrayList;
    private UsersListAdapter adapter;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private SharedPreferences sp;
    private LinearLayoutManager manager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_users, container, false);

        rv = view.findViewById(R.id.errorRv);
        rv.setHasFixedSize(true);
        manager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(manager);
        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("users");
        usersArrayList = new ArrayList<>();
        adapter = new UsersListAdapter(getActivity(),usersArrayList);
        rv.setAdapter(adapter);
        errorListing();
        return view;
    }
    public void errorListing(){
        Thread t = new Thread() {
            public void run() {
                //Query query = rootRef.orderByChild("errorId");
                rootRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        usersArrayList.clear();
                        for(DataSnapshot d: dataSnapshot.getChildren()){
                            Users users = d.getValue(Users.class);
                            users.setUserKey(d.getKey());
                            usersArrayList.add(users);

                        }
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        };
        t.start();
    }

}
