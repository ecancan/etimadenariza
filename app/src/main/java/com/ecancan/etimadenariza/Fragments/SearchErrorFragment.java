package com.ecancan.etimadenariza.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ecancan.etimadenariza.Adapters.ErrorListAdapter;
import com.ecancan.etimadenariza.Modules.Errors;
import com.ecancan.etimadenariza.Modules.Users;
import com.ecancan.etimadenariza.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;

public class SearchErrorFragment extends Fragment {
    private RecyclerView rv;
    private ArrayList<Errors> errorsArrayList;
    private ErrorListAdapter adapter;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private Users currentUsers;
    private SharedPreferences sp;
    private LinearLayoutManager manager;
    private EditText searchBox;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_error, container, false);
        searchBox = view.findViewById(R.id.searchBox);
        rv = view.findViewById(R.id.errorRv);
        rv.setHasFixedSize(true);
        manager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(manager);
        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("errors");


        errorsArrayList = new ArrayList<>();
        adapter = new ErrorListAdapter(getActivity(),errorsArrayList);
        rv.setAdapter(adapter);
        searchBox.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(!searchBox.getText().toString().isEmpty()){
                    errorListing(searchBox.getText().toString());
                }
                return false;
            }
        });
        return view;
    }
    public void errorListing(final String searchKey){
                Query query = rootRef.orderByChild("errorId");
                //.startAt(searchKey).endAt(searchKey+"\uf8ff")
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        errorsArrayList.clear();
                        if(Build.VERSION.SDK_INT == 19) {
                            getCurrentUser();
                        }
                        getCurrentUser();
                        for(DataSnapshot d: dataSnapshot.getChildren()){
                            Errors errors = d.getValue(Errors.class);
                            String shiftText = errors.getErrorShift();
                            if(errors.getErrorTitle().contains(searchKey)){
                                if(currentUsers.getUserLevel().equals("İşçi")){
                                    if(currentUsers.getShift().equals(shiftText)){
                                        errors.setErrorKey(d.getKey());
                                        errorsArrayList.add(errors);
                                    }
                                }else{
                                    errors.setErrorKey(d.getKey());
                                    errorsArrayList.add(errors);
                                }
                            }

                        }
                        Collections.reverse(errorsArrayList);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }
    public void getCurrentUser(){
        sp = getActivity().getSharedPreferences("currentUserInfo", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("cuser", "");
        currentUsers = gson.fromJson(json, Users.class);
    }

}
