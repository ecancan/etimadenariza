package com.ecancan.etimadenariza.Fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.ecancan.etimadenariza.Adapters.ErrorListAdapter;
import com.ecancan.etimadenariza.Modules.Errors;
import com.ecancan.etimadenariza.Modules.Users;
import com.ecancan.etimadenariza.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class ErrorFragment extends Fragment{
    private RecyclerView rv;
    private ArrayList<Errors> errorsArrayList;
    private ErrorListAdapter adapter;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private Users currentUsers;
    private SharedPreferences sp;
    private int currentPage = 1;
    private static final int loadItemCount = 500;
    private boolean isScrolling = false;
    private int currentItems,totalItems,scrollOutItems;
    private LinearLayoutManager manager;
    private ProgressBar progressBar;
    private String setMode = "normal";
    private Long setDate1;
    private Long setDate2;
    private int setDate1Int;
    private int setDate2Int;
    private String setStatus = "";
    private ImageView filterButton;
    private TextView dateRange;
    private TextView errorStatus;
    private ImageView dialogDate1Image;
    private ImageView dialogDate2Image;
    private EditText dialogDate1;
    private EditText dialogDate2;
    private Spinner dialogErrorStatus;
    private Button setFilter;
    private String humanDate1;
    private String humanDate2;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_error, container, false);
        dateRange = view.findViewById(R.id.dateRangeTextView);
        errorStatus = view.findViewById(R.id.errorStatusTextView);
        filterButton = view.findViewById(R.id.filterImageView);

        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.filter_dialog);
                dialog.setTitle("Arıza Filtreleme");
                dialogDate1 = dialog.findViewById(R.id.getDate1EditText);
                dialogDate2 = dialog.findViewById(R.id.getDate2EditText);
                //dialogDate1Image = dialog.findViewById(R.id.imageView4);
                //dialogDate2Image = dialog.findViewById(R.id.imageView5);
                setFilter = dialog.findViewById(R.id.button);


                dialogErrorStatus = dialog.findViewById(R.id.spinnerErrorFilter);
                ArrayAdapter<String> errorStatusAtapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.errorStatus));
                errorStatusAtapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                dialogErrorStatus.setAdapter(errorStatusAtapter);

                dialogDate1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar calendar = Calendar.getInstance();
                        int year = calendar.get(Calendar.YEAR);
                        int month = calendar.get(Calendar.MONTH);
                        int day = calendar.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                        month = month +1;
                                        humanDate1 = dayOfMonth+"/"+month+"/"+year;

                                        dialogDate1.setText(year +"-"+month+"-"+dayOfMonth+" 00:00:00");
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        String dialogGetDate1Text = dialogDate1.getText().toString();
                                        Date dateValue = null;
                                        try {
                                            dateValue = sdf.parse(dialogGetDate1Text);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        setDate1 = dateValue.getTime()/1000;
                                    }
                                }, year, month, day);

                        datePicker.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", datePicker);
                        datePicker.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", datePicker);
                        datePicker.show();
                    }
                });

                dialogDate2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar calendar = Calendar.getInstance();
                        int year = calendar.get(Calendar.YEAR);
                        int month = calendar.get(Calendar.MONTH);
                        int day = calendar.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                        month = month +1;
                                        humanDate2 = dayOfMonth+"/"+month+"/"+year;
                                        dialogDate2.setText(year +"-"+month+"-"+dayOfMonth+" 00:00:00");
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        String dialogGetDate2Text = dialogDate2.getText().toString();
                                        Date dateValue = null;
                                        try {
                                            dateValue = sdf.parse(dialogGetDate2Text);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        setDate2 = dateValue.getTime()/1000;
                                    }
                                }, year, month, day);
                        datePicker.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", datePicker);
                        datePicker.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", datePicker);
                        datePicker.show();

                    }
                });
                setFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setStatus = "Tüm Durumlar";
                        setMode = "normal";
                        dateRange.setText("Tüm Zamanlar");
                        errorStatus.setText(setStatus);
                        setDate1Int = 0;
                        setDate2Int = 0;
                        try{
                            setDate1Int = setDate1.intValue();
                            setDate2Int = setDate2.intValue();
                        }catch (Exception e){

                        }
                        System.out.println(setDate1Int +" "+ setDate2Int);

                        setStatus = dialogErrorStatus.getSelectedItem().toString();
                        if(!(dialogDate1.getText().toString().equals("Başlangıç Tarihi") && dialogDate2.getText().toString().equals("Bitiş Tarihi"))){
                            currentPage = 1;
                            setMode = "date";
                            errorListing(currentPage,setMode,setDate1Int,setDate2Int,errorStatusConverter(setStatus));
                            dateRange.setText(humanDate1+" - "+humanDate2);
                            errorStatus.setText(setStatus);
                        }else{
                            currentPage = 1;
                            setMode = "normal";
                            errorListing(currentPage,setMode,setDate1Int,setDate2Int,errorStatusConverter(setStatus));
                            if(setDate1Int != 0 && setDate2Int != 0){
                                dateRange.setText(humanDate1+" - "+humanDate2);
                            }else{
                                dateRange.setText("Tüm Zamanlar");
                            }
                            errorStatus.setText(setStatus);
                        }
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        rv = view.findViewById(R.id.errorRv);
        rv.setHasFixedSize(true);
        manager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(manager);
        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("errors");


        errorsArrayList = new ArrayList<>();
        adapter = new ErrorListAdapter(getActivity(),errorsArrayList);
        rv.setAdapter(adapter);

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = manager.getChildCount();
                totalItems = manager.getItemCount();
                scrollOutItems = manager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItems + scrollOutItems == totalItems)){
                    isScrolling = false;
                    currentPage++;
                    errorListing(currentPage,setMode,setDate1Int,setDate2Int,errorStatusConverter(setStatus));
                }
            }
        });

        errorListing(currentPage,setMode,setDate1Int,setDate2Int,errorStatusConverter(setStatus));
        return view;
    }
    public void errorListing(final int getCurrentPage,final String mode,final int date1,final int date2,final String status){
        Thread t = new Thread() {
            public void run() {
                Query query = rootRef;
                if(mode.equals("date")){
                    System.out.println(setDate1 +" ----- "+ setDate2);
                    query = rootRef.orderByChild("errorDateIntValue").startAt(date1).endAt(date2).limitToLast(getCurrentPage*loadItemCount);
                }else{
                    query = rootRef.orderByChild("errorId").limitToLast(getCurrentPage*loadItemCount);

                }

                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        errorsArrayList.clear();
                        if(Build.VERSION.SDK_INT == 19) {
                            getCurrentUser();
                        }
                        for(DataSnapshot d: dataSnapshot.getChildren()){
                            Errors errors = d.getValue(Errors.class);
                            String shiftText = errors.getErrorShift();
                            String unitText = errors.getErrorUnit();
                            if(currentUsers.getUserLevel().equals("İşçi")){
                                if(currentUsers.getShift().equals(shiftText)){
                                    if(!status.isEmpty()){
                                        if(errors.getErrorStatusByAdmin().equals(status)){
                                            errors.setErrorKey(d.getKey());
                                            errorsArrayList.add(errors);
                                        }
                                    }else{
                                        errors.setErrorKey(d.getKey());
                                        errorsArrayList.add(errors);
                                    }

                                }
                            }else if(currentUsers.getUserLevel().equals("Mühendis")){
                                    if(currentUsers.getUnit().equals(unitText)){
                                        if(!status.isEmpty()){
                                            if(errors.getErrorStatusByAdmin().equals(status)){
                                                errors.setErrorKey(d.getKey());
                                                errorsArrayList.add(errors);
                                            }
                                        }else{
                                            errors.setErrorKey(d.getKey());
                                            errorsArrayList.add(errors);
                                        }

                                    }
                            }else{
                                if(!status.isEmpty()){
                                    if(errors.getErrorStatusByAdmin().equals(status)){
                                        errors.setErrorKey(d.getKey());
                                        errorsArrayList.add(errors);
                                    }
                                }else{
                                    errors.setErrorKey(d.getKey());
                                    errorsArrayList.add(errors);
                                }
                            }

                        }
                        Collections.reverse(errorsArrayList);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        };
        t.start();
    }
    public String errorStatusConverter(String errorStatusInDB){

        switch (errorStatusInDB){
            case "Açık":
                return "open";
            case "Görevlendirildi":
                return "employment";
            case "İşlemde":
                return "wait";
            case "Kapalı":
                return "close";
                default: return "";
        }
    }
    public void getCurrentUser(){
        sp = getActivity().getSharedPreferences("currentUserInfo", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("cuser", "");
        currentUsers = gson.fromJson(json, Users.class);
    }
}
