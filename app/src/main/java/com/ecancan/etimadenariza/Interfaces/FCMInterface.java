package com.ecancan.etimadenariza.Interfaces;

import com.ecancan.etimadenariza.Modules.FCMModule;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface FCMInterface {
    @POST("send")
    Call<Response<FCMModule>> notifications(
            @HeaderMap Map<String, String> headers,
            @Body FCMModule notificationsMsg
    );

    void notifications(HashMap<String, String> headers);
}
